import os
import pathlib
import unittest
import numpy as np
from .context import WordSquare, WordFinder, Grid


class TestGrid(unittest.TestCase):
    def test_get_max_positions(self):
        word_size = 4
        amount_of_grid_positions = 16
        result = Grid.get_max_positions(word_size)
        self.assertEqual(amount_of_grid_positions, result)


class TestWordFinder(unittest.TestCase):

    def setUp(self) -> None:
        self.list_of_letters = ['a', 'a', 'c', 'c', 'd', 'e', 'e', 'e', 'e', 'm', 'm', 'n', 'n', 'n', 'o', 'o']
        self.result_list = ['aced', 'acme', 'acne', 'aeon', 'amen', 'ammo', 'anna', 'anoa', 'anon', 'caca', 'cade',
                            'came',
                            'cane', 'ceca', 'cede', 'coca', 'coco', 'coda', 'code', 'coed', 'coma', 'come', 'cone',
                            'conn',
                            'coon', 'dace', 'dada', 'dado', 'dame', 'damn', 'dead', 'dean', 'deco', 'deed', 'deem',
                            'deme',
                            'demo', 'dene', 'dodo', 'dome', 'dona', 'done', 'doom', 'eddo', 'mace', 'made', 'mama',
                            'mana',
                            'mane', 'mano', 'mead', 'mean', 'meed', 'memo', 'mend', 'meno', 'moan', 'mode', 'mome',
                            'mono',
                            'mood', 'moon', 'naan', 'nada', 'name', 'nana', 'need', 'neem', 'nema', 'nene', 'neon',
                            'node',
                            'noma', 'nome', 'nona', 'none', 'noon', 'odea', 'omen', 'once']
        self.wf = WordFinder(
            path_to_dictionary=str(pathlib.Path(os.path.abspath(__file__)).parents[1] / 'assets' / 'dictionary.txt'))

    def test_get_list_of_words(self):
        word_dictionary = self.wf.get_list_of_words(path_to_dictionary=str(pathlib.Path(os.path.abspath(__file__))
                                                                           .parents[1] / 'assets' / 'dictionary.txt'))
        self.assertIsNotNone(word_dictionary)
        self.assertEqual(len(word_dictionary), 172820)

    def test_word_contains_chars(self):
        word = 'moan'
        wcc = self.wf.word_contains_chars(word, self.list_of_letters)
        self.assertEqual(wcc, True)

    def test_get_matching_words(self):
        list_of_words = self.wf.get_matching_words(length_of_word=4, list_of_letters=self.list_of_letters)
        self.assertEqual(list_of_words, self.result_list)

    def test_matching_word_squares(self):
        word_square = ('moan', 'once', 'acme', 'need')
        match = self.wf.is_matching_word_square(word_square=word_square, list_of_characters=self.list_of_letters)
        self.assertEqual(match, True)

    def test_non_matching_word_squares(self):
        word_square = ('moan', 'once', 'acme', 'neem')
        match = self.wf.is_matching_word_square(word_square=word_square, list_of_characters=self.list_of_letters)
        self.assertEqual(match, False)


class TestWordSquare(unittest.TestCase):

    def test_find_word_squares(self):
        words = ['moan', 'need', 'acme', 'once']
        square = WordSquare.get_valid_word_squares(words)
        self.assertIsNotNone(square)
        self.assertEqual(next(square), ('moan', 'once', 'acme', 'need'))

    def test_create_wordsquare_grid(self):
        word_square = ('moan', 'once', 'acme', 'need')
        result = WordSquare.create_wordsquare_grid(word_square)
        word_square_grid = \
            [['moan'],
             ['once'],
             ['acme'],
             ['need']]
        np.testing.assert_array_equal(result, word_square_grid)


if __name__ == '__main__':
    unittest.main()
