******************
Naimuri Tech Test
******************

Executing main application

Within the src directory run the following to execute the application:

    python word_square.py ----grid_size 4 ----letters aaccdeeeemmnnnoo



Executing Test suite

Within the top level directory (naimuri_tech_test)
run the following to execute the test suite:

        python -m unittest

Notes:

* Application requires use of python 3
* Application requires Numpy (This can be seen within requirements.txt)


How the application words:

* The word square script is able to obtain the grid size and letter combinations and read it into the script
* The script validates that the the amount of letters supplied is equal to the grid spaces calculated by squaring the grid width
* The script then reads in the list of words from the dictionary file and adds all words that have a width equal to the grid width and are made up of only the letters contained within the letters list added as a command line argument when executing the script
* This list of words is then fed into the function for generating word squares which returns all the possible combinations of words which make up a word square equal to the grid size, in the correct order.
* For each generated word square combination, the script checks that it uses only the correct amount of letter combinations, generates a grid using these words and finally prints out the word square to the console.

Review:

Although the application matches the spec on the test document,
if I had more time I would optimise the code to ensure the script executes faster.
The script currently reduces in speed as the word square grid becomes larger.

Improvements

* An example of this would be looking into binary tree implementations and graph search algorithms, as the speed issue looks to be being caused by the get_valid_word_squares function.
* Another possible improvement would be in modifying the get_valid_word_squares function to implement concurrency. This could allow for the possible squares to be built up alongside each other, instead of having to wait until each possible square has been built before kicking off another.
