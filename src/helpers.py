import collections
import logging
from typing import List, Tuple
import numpy as np

logger = logging.getLogger(__name__)
stream_handler = logging.StreamHandler()
logger.addHandler(stream_handler)
logger.setLevel(level=logging.INFO)

__author__ = 'Paul Martin'


class Grid:

    @staticmethod
    def get_max_positions(n: int) -> int:
        positions = 0
        try:
            positions = n*n
        except Exception as ex:
            logger.exception(f"Exception Raised With: {ex}")
        return positions


class WordFinder:

    def __init__(self, path_to_dictionary):
        self.dictionary_word_list = self.get_list_of_words(path_to_dictionary)

    @staticmethod
    def get_list_of_words(path_to_dictionary: str) -> List[str]:
        with open(path_to_dictionary) as dictionary:
            dictionary_list = dictionary.read().strip().split()
        return dictionary_list

    def get_matching_words(self, length_of_word: int, list_of_letters: List[str]) -> List[str]:
        list_of_words = []
        try:
            for word in self.dictionary_word_list:
                if len(word) == length_of_word:
                    if self.word_contains_chars(word, list_of_letters):
                        list_of_words.append(word)
        except Exception as ex:
            logger.exception(f"Exception Raised With: {ex}")
        return list_of_words

    """
    Only add word if all letters in word are in list of letters
    """
    @staticmethod
    def word_contains_chars(word: str, list_of_characters: List[str]) -> bool:
        word_contains_letters = False
        try:
            for char in word:
                if char not in list_of_characters:
                    word_contains_letters = False
                    break
                word_contains_letters = True
        except Exception as ex:
            logger.exception(f"Exception Raised With: {ex}")

        return word_contains_letters

    @staticmethod
    def is_matching_word_square(word_square: tuple, list_of_characters: List[str]) -> bool:
        match = False
        word_square_characters = []
        try:
            for word in word_square:
                for character in word:
                    word_square_characters.append(character)
            if collections.Counter(word_square_characters) == collections.Counter(list_of_characters):
                match = True
        except Exception as ex:
            logger.exception(f"Exception Raised With: {ex}")
        return match


class WordSquare:

    @staticmethod
    def get_valid_word_squares(words: List[str]) -> tuple:
        try:
            possible_squares = set()
            words_by_letter_position = collections.defaultdict(set)
            """
                Once the loop has finished 'words_by_letter_position' will contain a dictionary with the word as the
                value and the letter and index of that letter as the key.
                This means that the dictionary contains the index of every letter in every word and cross-references
                that with the word itself
                
            {   
                {0, 'm'} : {'moan'},
                {0, 'o'} : {'once'},
                {1, 'o'} : {'moan'},
                {1, 'n'} : {'once'},
            }
            
            From here the function can view which words have a certain character in a certain position

            """
            for word in words:
                for index, letter in enumerate(word):
                    words_by_letter_position[(index, letter)].add(word)

                """
                Initialize a set of incomplete possible squares.
                Each item in the set is a tuple containing one of the words entered into the function.
                This is to store each word as the possible first word of the word square
                For example: (('moan',))
                """
                possible_squares.add((word,))

            while possible_squares:
                square = possible_squares.pop()
                """
                For every item (word) in the square, obtain the index of that item (word) in the square
                and the letter with the index matching the length of the square.
                For example, if the length is 1 and the word is moan, this will be 'o'
                These values are then stored as tuples in a list called keys
                
                If the word is moan for example and it is the first word in the square,
                the following tuple would be added to the list
                [(0, 'o')]
                
                If the word is once for example and it is the second word in the square,
                the following tuples would be added to the list:
                [(0, 'a'),(1, 'c')]
                
                """
                keys = [(i, square[i][len(square)]) for i in range(len(square))]
                """
                For every item in keys if there is an entry in the words_by_letter_position dictionary with that key,
                add the value (the set containing the word or words if there are multiple words that match)
                to the possible matches list.
                
                Example words_by_letter_position[(0, 'o')] == {'once', 'ones'}
                
                
                This is because the first letter in once and ones is o, which is subsequently the second letter in moan.
                As the loop continues, keys will contain more than one key. 
                For example if possible_squares contains two words keys will contain the first letter
                of the next word and the second letter of the next word and so on.
                
                This means that possible matches will contain two sets, one with all the words where the first letter
                is equal to the letter in the first key and another set which contains all the words where the
                second letter is is equal to the letter in the second key
                
                """
                possible_matches = [words_by_letter_position[key] for key in keys]

                """
                At this point the script uses a set intersection to pull out only words that live in all sets passed to it.
                This ensures the word meets all requirements set against the list of keys.
                
                The example follows on from before which means that the word will match both the first letter and the
                second letter and only then will the word be added to the valid square
                """
                for valid_word in set.intersection(*possible_matches):
                    valid_square = square + (valid_word,)
                    """                    
                    Yield valid square if it's complete (length of the set is equal to the grid width)
                    , or add the valid square as a possible square if not.
                    This will then ensure the while loop runs again and will continue to loop until the set contains
                    a valid word square
                    """
                    if len(valid_square) == len(word):
                        yield valid_square
                    else:
                        possible_squares.add(valid_square)
        except Exception as ex:
            logger.exception(f"Exception Raised With: {ex}")

    @staticmethod
    def print_formatted_word_square(wordsquare_grid: np.array):
        grid = ""
        try:
            print("Word Square:")
            for line in wordsquare_grid:
                print(''.join(line))
            print()
        except Exception as ex:
            logger.exception(f"Exception Raised With: {ex}")
            return grid

    @staticmethod
    def create_wordsquare_grid(word_square: tuple) -> np.array:
        wordsquare_grid = np.array(())
        try:
            arr = np.array(word_square)
            wordsquare_grid = arr.reshape(len(word_square), 1)
        except Exception as ex:
            logger.exception(f"Exception Raised With: {ex}")
        return wordsquare_grid
