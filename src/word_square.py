import argparse
import logging
import os
import pathlib
from helpers import WordFinder, WordSquare, Grid

logger = logging.getLogger(__name__)
stream_handler = logging.StreamHandler()
logger.addHandler(stream_handler)
logger.setLevel(level=logging.INFO)

__author__ = 'Paul Martin'


if __name__ == '__main__':
    try:
        parser = argparse.ArgumentParser(
            description="Word Square")
        parser.add_argument('--grid_width', required=True, help="Width of grid."
                                                               " Number is squared to obtain the number of grid places")
        parser.add_argument('--letters', required=True, help="String of letters")

        logger.info("Processing input args")
        args = parser.parse_args()
        grid_width = int(args.grid_width)
        logger.info(f"Grid Places = {grid_width} x {grid_width}")
        letters = args.letters
        logger.info(f"String of letters: {letters}")
        list_of_letters = [letter for letter in letters]
        if len(list_of_letters) == Grid.get_max_positions(grid_width):
            wf = WordFinder(path_to_dictionary=str(pathlib.Path(os.path.abspath(__file__)).parents[1] / 'assets' / 'dictionary.txt'))
            list_of_words = wf.get_matching_words(length_of_word=grid_width, list_of_letters=list_of_letters)
            for square in WordSquare.get_valid_word_squares(list_of_words):
                if wf.is_matching_word_square(square, list_of_letters):
                    word_square = WordSquare.create_wordsquare_grid(square)
                    WordSquare.print_formatted_word_square(word_square)
        else:
            logger.error("The length of the list of letters does not equal the amount of grid positions")
    except Exception as ex:
        logger.exception(f"Exception Raised With: {ex}")

