import os
import pathlib
import sys

sys.path.append(str(pathlib.Path(os.path.abspath(__file__)).parents[1] / 'src'))
from helpers import WordFinder, WordSquare, Grid
